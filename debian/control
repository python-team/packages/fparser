oource: fparser
Section: python
Priority: optional
Maintainer: Alastair McKinstry <mckinstry@debian.org>
Build-Depends: debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-pytest,
 python3-setuptools,
 python3-setuptools-scm
Standards-Version: 4.7.0
Homepage: https://github.com/stfc/fparser
Vcs-Git: https://salsa.debian.org/python-team/modules/fparser.git -b debian/latest
Vcs-Browser: https://salsa.debian.org/python-team/modules/fparser

Package: python3-fparser
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Python3 Parser for the Fortran language
 The fparser package is a Python implementation of a Fortran 66/77/90/95/2003
 language parser. Currently fparser actually includes two different
 implementations; the first, “fparser”, supports Fortran 66/77/90 (plus some
 extensions — see fparser) but only parses down to the level of individual
 lines of code, i.e. not the content of the Fortran expressions themselves.
 The second, “fparser2”, also has support for Fortran 2003 and is able to parse
 Fortran expressions. It is however, less mature than fparser.
